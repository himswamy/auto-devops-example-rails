# Auto-devops Example App for rails

This is a bare bones rails app to demo auto-devops. It was created with the
steps below. 

Step 1. Create a new rails app using PostgreSQL 

`rails new auto-devops --database=postgresql && cd auto-devops`

Step 2. Start PostgreSQL

`brew services start postgresql # on OSX`

Step 3. Create db/schema.rb

`rake db:create && rake db:migrate`

Step 4. Add a route for / 

`rails generate controller Welcome index`

Then add the line `root to: "home#index` to `config/routes.rb`

Step 5. Push to GitLab

`git push --set-upstream git@gitlab.com:<username>/auto-devops-example-rails.git master`
